#include "Player.h"
#include "MessageLog.h"

// private methods

void Player::IncrementMoveCount()
{
    moveCount++;

    // deduct food every 2 moves

	if (moveCount % 2 == 0)
	{
		food--;
		foodDirty = true;
	}
}


// public methods

Player::Player(int x, int y)
	: x(x), y(y), transport(Foot),
		weapon(Hands), armour(Skin), hits(150), food(200),
		experience(0), coin(100), hitsDirty(true), foodDirty(true),
		experienceDirty(true), coinDirty(true)
{
}

Player::~Player()
{
}

PlayerTransport Player::GetTransport() const
{
    return transport;
}

int Player::GetX() const
{
    return x;
}

int Player::GetY() const
{
    return y;
}

void Player::MoveLeft()
{
    x--;
    if (x < 0)
        x = 167;

    IncrementMoveCount();
}

void Player::MoveRight()
{
    x++;
    if (x > 167)
        x = 0;

    IncrementMoveCount();
}

void Player::MoveUp()
{
    y--;
    if (y < 0)
        y = 155;

    IncrementMoveCount();
}

void Player::MoveDown()
{
    y++;
    if (y > 155)
        y = 0;

    IncrementMoveCount();
}

void Player::Pass()
{
    IncrementMoveCount();
}

void Player::CycleTransportLeft()
{
    switch (transport)
    {
    case Foot:
        transport = TimeMachine;
        break;
    case Horse:
        transport = Foot;
        break;
    case Cart:
        transport = Horse;
        break;
    case Raft:
        transport = Cart;
        break;
    case Frigate:
        transport = Raft;
        break;
    case Aircar:
        transport = Frigate;
        break;
    case Shuttle:
        transport = Aircar;
        break;
    case TimeMachine:
        transport = Shuttle;
        break;
    }
}

void Player::CycleTransportRight()
{
    switch (transport)
    {
        case Foot:
            transport = Horse;
            break;
        case Horse:
            transport = Cart;
            break;
        case Cart:
            transport = Raft;
            break;
        case Raft:
            transport = Frigate;
            break;
        case Frigate:
            transport = Aircar;
            break;
        case Aircar:
            transport = Shuttle;
            break;
        case Shuttle:
            transport = TimeMachine;
            break;
        case TimeMachine:
            transport = Foot;
            break;
    }
}

unsigned short Player::GetHits() const
{
    return hits;
}

unsigned short Player::GetFood() const
{
    return food;
}

unsigned short Player::GetExperience() const
{
    return experience;
}

unsigned short Player::GetCoin() const
{
    return coin;
}

bool Player::GetAndRemoveHitsDirty()
{
	bool wasHitsDirty = hitsDirty;
	hitsDirty = false;
	return wasHitsDirty;
}

bool Player::GetAndRemoveFoodDirty()
{
	bool wasFoodDirty = foodDirty;
	foodDirty = false;
	return wasFoodDirty;
}

bool Player::GetAndRemoveExperienceDirty()
{
	bool wasExperienceDirty = experienceDirty;
	experienceDirty = false;
	return wasExperienceDirty;
}

bool Player::GetAndRemoveCoinDirty()
{
	bool wasCoinDirty = coinDirty;
	coinDirty = false;
	return wasCoinDirty;
}
