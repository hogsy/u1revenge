#ifndef _GAMEENGINE_H_
#define _GAMEENGINE_H_

#include <memory>
#include <SDL.h>
#include <SDL_ttf.h>

#include "Palette.h"
#include "Tileset.h"
#include "MessageLogView.h"
#include "ViewportView.h"
#include "BottomRightView.h"
#include "BorderView.h"
#include "GameSettings.h"
#include "Sdl2Core.h"
#include "SosariaInputController.h"

using std::unique_ptr;

class GameEngine
{
private:
    int width;
    int height;
    Tileset* tileset;
    TTF_Font* font;
    BorderView* borderView;
    MessageLogView* messageLogView;
    BottomRightView* bottomRightView;
    ViewportView* viewportView;
    GameSettings gameSettings;
	Sdl2Core* sdl2Core;
	SosariaInputController* inputController;
public:
    GameEngine(Sdl2Core* sdl2Core, int width, int height, int scale, string u1Path,
        MessageLog* messageLog, World* world, int viewportWidth, int viewportHeight,
        Player* player, GameSettings& gameSettings);
    ~GameEngine();
    void Render() const;
	bool ProcessInputEvent(SDL_Event& event);
};

#endif