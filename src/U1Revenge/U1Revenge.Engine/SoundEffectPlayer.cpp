#include <SDL.h>

#include "SoundEffect.h"
#include "SoundEffectPlayer.h"

// private methods

void SoundEffectPlayer::PlaySoundEffect(SoundEffect& soundEffect)
{
	// TODO error handling

	int success = SDL_QueueAudio(deviceId, soundEffect.GetWavBuffer(), soundEffect.GetWavLength());
}

// public methods

SoundEffectPlayer::SoundEffectPlayer()
	: moveGrassSound("move-grass.wav")
{
	// open audio device // TODO error handling

	deviceId = SDL_OpenAudioDevice(NULL, 0, &moveGrassSound.GetWavSpec(), NULL, 0);
	SDL_PauseAudioDevice(deviceId, 0);
}

SoundEffectPlayer::~SoundEffectPlayer()
{
	SDL_CloseAudioDevice(deviceId);
}

void SoundEffectPlayer::PlayMoveGrassSound()
{
	PlaySoundEffect(moveGrassSound);
}
