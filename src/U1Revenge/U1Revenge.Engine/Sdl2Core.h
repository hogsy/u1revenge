#ifndef _SDL2CORE_H_
#define _SDL2CORE_H_

#include <SDL.h>

class Sdl2Core
{
private:
	bool fullScreen;
	SDL_Window* window;
	SDL_Renderer* renderer;
	SDL_Surface* icon;
public:
	Sdl2Core(int width, int height, int scale);
	~Sdl2Core();
	SDL_Renderer* GetRenderer() const;
	void ToggleFullScreen();
	void SetWindowIcon(int* iconPixels);
};

#endif