#ifndef _COLLISIONDETECTOR_H_
#define _COLLISIONDETECTOR_H_

#include "World.h"
#include "Player.h"

class CollisionDetector
{
private:
	bool enabled;
	World world;
	Player player;
public:
	CollisionDetector(World& world, Player& player);
	~CollisionDetector();
};

#endif