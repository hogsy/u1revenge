#include "TilesetFactory.h"
#include "CgaTileset.h"
#include "T1kTileset.h"
#include "EgaTileset.h"

TilesetFactory::TilesetFactory()
{
}


TilesetFactory::~TilesetFactory()
{
}

Tileset * TilesetFactory::CreateTileset(string graphicsMode)
{
    if (graphicsMode == "CGA")
        return new CgaTileset();
    else if (graphicsMode == "T1K")
        return new T1kTileset();
    else if (graphicsMode == "EGA")
        return new EgaTileset();
    else
        return nullptr;
}
