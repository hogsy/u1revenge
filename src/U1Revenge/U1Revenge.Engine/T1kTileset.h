#ifndef _T1K_TILESET_H_
#define _T1K_TILESET_H_

#include "Tileset.h"
#include "T1kPalette.h"

class T1kTileset : public Tileset
{
private:
    T1kPalette palette;
public:
    T1kTileset();
    ~T1kTileset();
    virtual void Load(const char* filePath);
};

#endif