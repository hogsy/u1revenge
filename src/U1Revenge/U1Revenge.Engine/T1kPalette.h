#ifndef _T1KPALETTE_H_
#define _T1KPALETTE_H_

#include "Palette.h"

class T1kPalette : public Palette
{
public:
    T1kPalette();
    ~T1kPalette();
    virtual int IndexToColour(char index);
};

#endif