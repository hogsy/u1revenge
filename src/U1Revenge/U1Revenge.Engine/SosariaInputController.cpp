#include <SDL.h>

#include "SosariaInputController.h"

SosariaInputController::SosariaInputController(Sdl2Core* sdl2Core, Player* player,
    World* world, MessageLog* messageLog, Tileset* tileset)
    : sdl2Core(sdl2Core), player(player), world(world), messageLog(messageLog),
		tileset(tileset), soundEffectPlayer()
{
}

SosariaInputController::~SosariaInputController()
{
}

bool SosariaInputController::ProcessInputEvent(SDL_Event& event)
{
    switch (event.type)
    {
        case SDL_QUIT:
            return false;
        case SDL_KEYDOWN:
            switch (event.key.keysym.sym)
            {
                case SDLK_LEFT:
                    player->MoveLeft();
                    messageLog->WriteLine("West");
					soundEffectPlayer.PlayMoveGrassSound();
                    break;
                case SDLK_RIGHT:
                    player->MoveRight();
                    messageLog->WriteLine("East");
					soundEffectPlayer.PlayMoveGrassSound();
                    break;
                case SDLK_UP:
                    player->MoveUp();
                    messageLog->WriteLine("North");
					soundEffectPlayer.PlayMoveGrassSound();
                    break;
                case SDLK_DOWN:
                    player->MoveDown();
                    messageLog->WriteLine("South");
					soundEffectPlayer.PlayMoveGrassSound();
                    break;
                case SDLK_SPACE:
                    player->Pass();
                    messageLog->WriteLine("Pass");
                    break;
				case SDLK_F8:
					SaveScreenshot();
					messageLog->WriteLine("Screenshot taken.");
					break;
				case SDLK_F9:
					messageLog->Write("Saving map...");
					SaveMap();
					messageLog->WriteLine(" done.");
					break;
                case SDLK_F10:
                    sdl2Core->ToggleFullScreen();
                    break;
                case SDLK_LEFTBRACKET:
                    player->CycleTransportLeft();
                    break;
                case SDLK_RIGHTBRACKET:
                    player->CycleTransportRight();
                    break;
                case SDLK_i:
                {
                    messageLog->WriteLine("Inform and search");
                    const char* name = world->GetLocationName(player->GetX(), player->GetY());
                    messageLog->WriteLine(name);
                    break;
                }
            }
            break;
    }

    return true;
}

void SosariaInputController::SaveMap()
{
	const int worldWidth = 168;
	const int worldHeight = 156;

	int surfacePitch = worldWidth * 16 * sizeof(int); // length of one row in bytes
	vector<int> mapPixels(surfacePitch * worldHeight * 16);

	for (int i = 0; i < worldHeight; i++)
	{
		for (int j = 0; j < worldWidth; j++)
		{
			int tileIndex = world->GetTileIndexAt(j, i);

			// adjust tileIndex because some tiles are just alternate frames
			// of the previous tile, i.e. city and castle tiles have alternate
			// flag positions

			if (tileIndex > 4)
				tileIndex++;
			if (tileIndex > 7)
				tileIndex++;

			int* tilePixels = tileset->GetPixels(tileIndex);

			for (int a = 0; a < 16; a++)
			{
				for (int b = 0; b < 16; b++)
				{
					int key = (surfacePitch * (i * 16 + a) / 4) + (j * 16 + b);
					mapPixels[key] = tilePixels[a * 16 + b];
				}
			}
		}
	}
	
	int* surfaceData = mapPixels.data();
	int surfaceWidth = worldWidth * 16;
	int surfaceHeight = worldHeight * 16;
	int surfaceDepth = 32;
	Uint32 surfaceFormat = SDL_PIXELFORMAT_ARGB8888;
	SDL_Surface* mapSurface = SDL_CreateRGBSurfaceWithFormatFrom(surfaceData,
		surfaceWidth, surfaceHeight, surfaceDepth, surfacePitch, surfaceFormat);

	SDL_SaveBMP(mapSurface, "map.bmp");

	SDL_FreeSurface(mapSurface);
}

void SosariaInputController::SaveScreenshot()
{
	const Uint32 format = SDL_PIXELFORMAT_ARGB8888;
	const int width = 640; // TODO adapt according to settings
	const int height = 400; // TODO adapt according to settings
	auto renderer = sdl2Core->GetRenderer();

	SDL_Surface *surface = SDL_CreateRGBSurfaceWithFormat(0, width, height, 32, format);
	SDL_RenderReadPixels(renderer, NULL, format, surface->pixels, surface->pitch);
	SDL_SaveBMP(surface, "screenshot.bmp");
	SDL_FreeSurface(surface);
}

