#include "MessageLogView.h"

MessageLogView::MessageLogView(MessageLog* messageLog, SDL_Renderer* renderer, TTF_Font* font)
    : View(renderer), messageLog(messageLog), fontColour({ 84, 255, 255 }),
        lineSurfaces(4), lineTextures(4), font(font)
{
	for (int i = 0; i < 4; i++)
	{
		lineSurfaces[i] = nullptr;
		lineTextures[i] = nullptr;
	}
}

MessageLogView::~MessageLogView()
{
	Cleanup();
}

void MessageLogView::Render()
{
	bool needsRendering = messageLog->GetAndRemoveDirty();
	auto renderer = GetRenderer();

	if (needsRendering)
	{
		Cleanup();

		vector<string> lines = *messageLog->GetMessages();

		for (int i = 0; i < 4; i++)
		{
			lineSurfaces[i] = TTF_RenderText_Solid(font, lines[i].c_str(), fontColour);
			lineTextures[i] = SDL_CreateTextureFromSurface(renderer, lineSurfaces[i]);
		}
	}

	for (int i = 0; i < 4; i++)
	{
		SDL_Rect dstrect = { 8, 336 + (i * 16), 0, 0 };
		SDL_QueryTexture(lineTextures[i], NULL, NULL, &dstrect.w, &dstrect.h);

		SDL_RenderCopy(renderer, lineTextures[i], NULL, &dstrect);
	}
}

void MessageLogView::Cleanup()
{
	for (int i = 0; i < 4; i++)
	{
		SDL_DestroyTexture(lineTextures[i]);
		SDL_FreeSurface(lineSurfaces[i]);
	}
}
