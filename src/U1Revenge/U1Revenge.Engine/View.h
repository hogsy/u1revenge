#ifndef _VIEW_H_
#define _VIEW_H_

#include <SDL.h>

class View
{
private:
    SDL_Renderer* renderer;
protected:
    SDL_Renderer* GetRenderer();
public:
    View(SDL_Renderer* renderer);
    ~View();
    virtual void Render() = 0;
};

#endif