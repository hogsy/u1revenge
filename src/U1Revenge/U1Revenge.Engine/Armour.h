#ifndef _ARMOUR_H_
#define _ARMOUR_H_

enum Armour
{
    Skin = 0,
    LeatherArmor = 1,
    ChainMail = 2,
    PlateMail = 3,
    VacuumSuit = 4,
    ReflectSuit = 5
};

#endif