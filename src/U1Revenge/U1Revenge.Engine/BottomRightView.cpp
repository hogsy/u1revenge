#include <string>

#include "BottomRightView.h"

// private methods

void BottomRightView::SetupStatic(SDL_Texture*& texture, const char* str, SDL_Rect& rect)
{
    auto renderer = GetRenderer();

    SDL_Surface* surface = TTF_RenderText_Solid(font, str, fontColour);
    texture = SDL_CreateTextureFromSurface(renderer, surface);
	SDL_FreeSurface(surface);

    SDL_QueryTexture(texture, NULL, NULL, &rect.w, &rect.h);

    SDL_RenderCopy(renderer, texture, NULL, &rect);
}

void BottomRightView::RenderDynamic(SDL_Texture*& texture, bool dirty, unsigned short value, SDL_Color colour, int x, int y)
{
    auto renderer = GetRenderer();

	if (dirty)
	{
		std::string valueStr = std::to_string(value);

		// right-align
		if (value < 10)
			valueStr = " " + valueStr;
		if (value < 100)
			valueStr = " " + valueStr;
		if (value < 1000)
			valueStr = " " + valueStr;

		SDL_DestroyTexture(texture);

		SDL_Surface* surface = TTF_RenderText_Solid(font, valueStr.c_str(), colour);
		texture = SDL_CreateTextureFromSurface(renderer, surface);
		SDL_FreeSurface(surface);
	}

    SDL_Rect dstrect = { x, y, 0, 0 };
    SDL_QueryTexture(texture, NULL, NULL, &dstrect.w, &dstrect.h);

    SDL_RenderCopy(renderer, texture, NULL, &dstrect);
}

// public methods

BottomRightView::BottomRightView(SDL_Renderer* renderer, TTF_Font* font, Player* player, int scale)
    : View(renderer), font(font), player(player), scale(scale), fontColour({ 84, 255, 255 }),
        redFontColour({ 255, 80, 81 }), valueOffset(80),
        hitsKeyFontTexture(nullptr), hitsValueTexture(nullptr), hitsRect({ 248 * scale, 168 * scale, 0, 0 }),
        foodKeyFontTexture(nullptr), foodValueTexture(nullptr), foodRect({ 248 * scale, 176 * scale, 0, 0 }),
        expKeyFontTexture(nullptr), expValueTexture(nullptr), expRect({ 248 * scale, 184 * scale, 0, 0 }),
        coinKeyFontTexture(nullptr), coinValueTexture(nullptr), coinRect({ 248 * scale, 192 * scale, 0, 0 })
{
    SetupStatic(hitsKeyFontTexture, "Hits:", hitsRect);
    SetupStatic(foodKeyFontTexture, "Food:", foodRect);
    SetupStatic(expKeyFontTexture, "Exp.:", expRect);
    SetupStatic(coinKeyFontTexture, "Coin:", coinRect);
}

BottomRightView::~BottomRightView()
{
    CleanupStatic();
	CleanupDynamic();
}

void BottomRightView::Render()
{
    auto renderer = GetRenderer();

    // render static fields (Hits, Food, Exp. and Coin)

    SDL_RenderCopy(renderer, hitsKeyFontTexture, NULL, &hitsRect);
    SDL_RenderCopy(renderer, foodKeyFontTexture, NULL, &foodRect);
    SDL_RenderCopy(renderer, expKeyFontTexture, NULL, &expRect);
    SDL_RenderCopy(renderer, coinKeyFontTexture, NULL, &coinRect);

    // render dynamic values

    unsigned short hits = player->GetHits();
    unsigned short food = player->GetFood();
    unsigned short experience = player->GetExperience();
    unsigned short coin = player->GetCoin();

	bool hitsDirty = player->GetAndRemoveHitsDirty();
	bool foodDirty = player->GetAndRemoveFoodDirty();
	bool expDirty = player->GetAndRemoveExperienceDirty();
	bool coinDirty = player->GetAndRemoveCoinDirty();
    
    RenderDynamic(hitsValueTexture, hitsDirty, hits, hits < 100 ? redFontColour : fontColour, hitsRect.x + valueOffset, hitsRect.y);
    RenderDynamic(foodValueTexture, foodDirty, food, food < 100 ? redFontColour : fontColour, foodRect.x + valueOffset, foodRect.y);
    RenderDynamic(expValueTexture, expDirty, experience, fontColour, expRect.x + valueOffset, expRect.y);
    RenderDynamic(coinValueTexture, coinDirty, coin, fontColour, coinRect.x + valueOffset, coinRect.y);
}

void BottomRightView::CleanupStatic()
{
    SDL_DestroyTexture(hitsKeyFontTexture);
    SDL_DestroyTexture(foodKeyFontTexture);
    SDL_DestroyTexture(expKeyFontTexture);
    SDL_DestroyTexture(coinKeyFontTexture);
}

void BottomRightView::CleanupDynamic()
{
	SDL_DestroyTexture(hitsValueTexture);
	SDL_DestroyTexture(foodValueTexture);
	SDL_DestroyTexture(expValueTexture);
	SDL_DestroyTexture(coinValueTexture);
}
