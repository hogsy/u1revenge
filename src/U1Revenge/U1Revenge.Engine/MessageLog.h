#ifndef _MESSAGELOG_H_
#define _MESSAGELOG_H_

#include <vector>
#include <string>

using std::vector;
using std::string;

class MessageLog
{
private:
	bool isDirty;
    vector<string> lines;
    void PushPrivate(string message);
public:
    MessageLog();
    ~MessageLog();
    void WriteLine(const char * message);
	void Write(const char * message);
	vector<string>* GetMessages();
	bool GetAndRemoveDirty();
};

#endif
