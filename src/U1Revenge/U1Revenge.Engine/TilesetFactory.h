#ifndef _TILESETFACTORY_H_
#define _TILESETFACTORY_H_

#include <string>

#include "Tileset.h"

using std::string;

class TilesetFactory
{
public:
    TilesetFactory();
    ~TilesetFactory();
    Tileset* CreateTileset(string graphicsMode);
};

#endif