#ifndef _CGAPALETTE_H_
#define _CGAPALETTE_H_

#include "Palette.h"

class CgaPalette : public Palette
{
public:
    CgaPalette();
    ~CgaPalette();
    virtual int IndexToColour(char index);
};

#endif