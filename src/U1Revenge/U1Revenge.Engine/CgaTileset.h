#ifndef _CGA_TILESET_H_
#define _CGA_TILESET_H_

#include "Tileset.h"
#include "CgaPalette.h"

class CgaTileset : public Tileset
{
private:
    CgaPalette palette;
public:
    CgaTileset();
    ~CgaTileset();
    virtual void Load(const char* filePath);
};

#endif