#ifndef _SOSARIAINPUTCONTROLLER_H_
#define _SOSARIAINPUTCONTROLLER_H_

#include <memory>
#include <SDL.h>

#include "Sdl2Core.h"
#include "Player.h"
#include "World.h"
#include "MessageLog.h"
#include "Tileset.h"
#include "SoundEffectPlayer.h"

using std::unique_ptr;

class SosariaInputController
{
private:
	Sdl2Core* sdl2Core;
    Player* player;
    World* world;
    MessageLog* messageLog;
	Tileset* tileset;
	SoundEffectPlayer soundEffectPlayer;
public:
    SosariaInputController(Sdl2Core* sdl2Core, Player* player,
        World* world, MessageLog* messageLog, Tileset* tileset);
    ~SosariaInputController();
    bool ProcessInputEvent(SDL_Event& event);
	void SaveMap();
	void SaveScreenshot();
};

#endif