#include <iostream>
#include <SDL_ttf.h>

#include "Sdl2Core.h"

Sdl2Core::Sdl2Core(int width, int height, int scale)
	: fullScreen(false), window(nullptr), renderer(nullptr), icon(nullptr)
{
	SDL_Init(SDL_INIT_VIDEO | SDL_INIT_AUDIO);
	TTF_Init();

	window = SDL_CreateWindow("U1Revenge Engine 3",
		SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, width * scale, height * scale, 0);

	renderer = SDL_CreateRenderer(window, -1, 0);
}

Sdl2Core::~Sdl2Core()
{
	SDL_FreeSurface(icon);
	SDL_DestroyRenderer(renderer);
	SDL_DestroyWindow(window);

	TTF_Quit();
	SDL_Quit();
}

SDL_Renderer * Sdl2Core::GetRenderer() const
{
	return renderer;
}

void Sdl2Core::ToggleFullScreen()
{
	fullScreen = !fullScreen;
	SDL_SetWindowFullscreen(window, fullScreen ? SDL_WINDOW_FULLSCREEN : 0);
}

void Sdl2Core::SetWindowIcon(int * iconPixels)
{
	int iconPixelsWithTransparency[256];
	memcpy(iconPixelsWithTransparency, iconPixels, 256 * sizeof(int));

	for (int i = 0; i < 256; i++) // set all black pixels to transparent
	{
		if (iconPixelsWithTransparency[i] == 0xFF000000)
			iconPixelsWithTransparency[i] = 0x00000000; // turn off opacity, i.e. alpha = 0
	}

	icon = SDL_CreateRGBSurfaceWithFormatFrom(iconPixelsWithTransparency, 16, 16, 32,
		16 * sizeof(int), SDL_PIXELFORMAT_ARGB8888);
	SDL_SetWindowIcon(window, icon);
}
