#ifndef _TILESET_H_
#define _TILESET_H_

class Tileset
{
protected:
    int* pixels;
    static const int tileWidth = 16;
    static const int tileHeight = 16;
    static const int tileCount = 52;
public:
    Tileset();
    virtual ~Tileset();
    virtual void Load(const char* filePath) = 0;
    int* GetPixels(char tileIndex) const;
};

#endif