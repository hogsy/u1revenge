#ifndef _MESSAGELOGVIEW_H_
#define _MESSAGELOGVIEW_H_

#include <SDL.h>
#include <SDL_ttf.h>

#include "View.h"
#include "MessageLog.h"

class MessageLogView : public View
{
private:
    MessageLog* messageLog;
    SDL_Color fontColour;
	vector<SDL_Surface*> lineSurfaces;
	vector<SDL_Texture*> lineTextures;
    TTF_Font* font;
    SDL_Rect* viewportRect;
public:
    MessageLogView(MessageLog* messageLog, SDL_Renderer* renderer, TTF_Font* font);
    ~MessageLogView();
    void Render();
	void Cleanup();
};

#endif