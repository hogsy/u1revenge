#ifndef _WORLD_H_
#define _WORLD_H_

#include <map>

#include "Location.h"

class World
{
private:
    char* cells;
    std::map<int, Location*> locations;

    void AddLocation(Location* location);
public:
    World();
    ~World();
    void Load(const char* filePath);
    char GetTileIndexAt(int x, int y) const;
    const char* GetLocationName(int x, int y) const;
};

#endif