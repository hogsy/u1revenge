#include <fstream>
#include <vector>

#include "T1kTileset.h"
#include "ErrorOut.h"

T1kTileset::T1kTileset()
{
}

T1kTileset::~T1kTileset()
{
}

void T1kTileset::Load(const char* filePath)
{
    const int fileSize = 6656;
    std::ifstream file(filePath, std::ios::binary | std::ios::in);
    std::vector<char> buffer(fileSize);

    int pixelIndex = 0;

    if (file.read(&buffer[0], fileSize))
    {
        // convert file bytes to pixel colour array

        for (int i = 0; i < fileSize; i++) // goes through each byte
        {
            char currentByte = buffer[i];
            char paletteIndex = 0; // this is the pixel value, to be translated to colour later

            for (int j = 0; j < 2; j++) // determines whether to take high or low half-byte
            {
                if (j == 0)
                    paletteIndex = (currentByte & 0xF0) >> 4;
                else
                    paletteIndex = currentByte & 0x0F;

                int pixelColour = palette.IndexToColour(paletteIndex);
                pixels[pixelIndex] = pixelColour;
                pixelIndex++;
            }
        }
    }
    else
    {
        ErrorOut("Unable to open Ultima 1 tileset " << filePath);
    }

    file.close();
}