#include "GameSettings.h"
#include "GameEngine.h"
#include "MessageLog.h"
#include "Player.h"
#include "Palette.h"
#include "Tileset.h"
#include "World.h"
#include "SosariaInputController.h"

int main(int argc, char** argv)
{
    GameSettings gameSettings;
    gameSettings.Load("gamesettings.txt");

    string u1Path = gameSettings.Get("U1Path", "C:/games/Ultima 1");
	int scale = gameSettings.GetInt("Scale", 2);

    const int width = 320;
    const int height = 200;
    const int viewportWidth = 19;
    const int viewportHeight = 9;
    SDL_Event event;

    std::string mapFilePath = u1Path + "/MAP.BIN";
    World world;
    world.Load(mapFilePath.c_str());

    MessageLog messageLog;
    messageLog.WriteLine("Welcome to Ultima 1!");
    Player player(40, 40);

	Sdl2Core sdl2Core(width, height, scale);

    GameEngine gameEngine(&sdl2Core, width, height, scale, u1Path, &messageLog, &world,
        viewportWidth, viewportHeight, &player, gameSettings);

    while (true)
    {
        SDL_Delay(100);
        SDL_PollEvent(&event);

        bool proceed = gameEngine.ProcessInputEvent(event);
        if (!proceed)
            break;

        gameEngine.Render();
    }

    return 0;
}