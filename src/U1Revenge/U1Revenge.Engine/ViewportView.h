#ifndef _VIEWPORTVIEW_H_
#define _VIEWPORTVIEW_H_

#include "View.h"
#include "Tileset.h"
#include "World.h"
#include "Player.h"

class ViewportView : public View
{
private:
    int width;
    int height;
    int viewportWidth;
    int viewportHeight;
    Uint32* pixels;
    SDL_Texture* texture;
    SDL_Rect viewportRect;
    World* world;
    Tileset* tileset;
    Player* player;

    void DrawTile(const Tileset& tileset, const char tileIndex, Uint32* pixels,
        int viewportX, int viewportY);
    void DrawViewport(const World& world, const Tileset& tileset, const Player& player,
        int viewportWidth, int viewportHeight, Uint32* pixels);
public:
    ViewportView(SDL_Renderer* renderer, int width, int height,
        int viewportWidth, int viewportHeight, World* world, Tileset* tileset,
        Player* player);
    ~ViewportView();
    void Render();
};

#endif