#ifndef _BOTTOMRIGHTVIEW_H_
#define _BOTTOMRIGHTVIEW_H_

#include <SDL.h>
#include <SDL_ttf.h>

#include "View.h"
#include "Player.h"

class BottomRightView : public View
{
private:
    TTF_Font* font;
    Player* player;
    int scale;
    SDL_Color fontColour;
    SDL_Color redFontColour;
    int valueOffset; // x-offset between position of a key (e.g. "Hits:" and its value)

    SDL_Texture* hitsKeyFontTexture;
    SDL_Rect hitsRect;
	SDL_Texture* hitsValueTexture;

    SDL_Texture* foodKeyFontTexture;
    SDL_Rect foodRect;
	SDL_Texture* foodValueTexture;

    SDL_Texture* expKeyFontTexture;
    SDL_Rect expRect;
	SDL_Texture* expValueTexture;

    SDL_Texture* coinKeyFontTexture;
    SDL_Rect coinRect;
	SDL_Texture* coinValueTexture;

    void SetupStatic(SDL_Texture*& texture, const char* str, SDL_Rect& rect);
    void RenderDynamic(SDL_Texture*& texture, bool dirty, unsigned short value, SDL_Color colour, int x, int y);
public:
    BottomRightView(SDL_Renderer* renderer, TTF_Font* font, Player* player, int scale);
    ~BottomRightView();
    void Render();
    void CleanupStatic();
	void CleanupDynamic();
};

#endif