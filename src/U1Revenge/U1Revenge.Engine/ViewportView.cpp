#include <iostream>

#include "ViewportView.h"

void ViewportView::DrawTile(const Tileset& tileset, const char tileIndex, Uint32* pixels,
    int viewportX, int viewportY)
{
    int* tilePixels = tileset.GetPixels(tileIndex);
    int* shiftedPixels = nullptr;

    if (tileIndex == 0) // water - animate by shifting rows
    {
        Uint32 ticks = SDL_GetTicks();
        Uint32 interval = ticks / 250;
        int frame = interval % 4;

        if (frame > 0)
        {
            // concept: (each whole line is a tile, and each dash represents a row)
            // |----------------|
            //  |----------------|
            //   |----------------|
            //    |----------------|
        
            shiftedPixels = new int[256]; // 16 x 16
            int totalTilePixels = 16 * 16;
            int pixelsShifted = 16 * frame; // 16 is the number of pixels in a row
        
            //         A       B
            // |--------------|--|  source
            //
            // |--|--------------|  destination
            //  B         A
        
            memcpy(shiftedPixels + pixelsShifted, tilePixels, (totalTilePixels - pixelsShifted) * sizeof(int)); // A
            memcpy(shiftedPixels, tilePixels + totalTilePixels - pixelsShifted, pixelsShifted * sizeof(int)); // B
        
            tilePixels = shiftedPixels;
        }
    }

    for (int i = 0; i < 16; i++)
    {
        for (int j = 0; j < 16; j++)
        {
            pixels[(viewportY * 16 + i) * 320 + j + (viewportX * 16)] = tilePixels[i * 16 + j];
        }
    }

    delete[] shiftedPixels;
}

void ViewportView::DrawViewport(const World& world, const Tileset& tileset, const Player& player,
    int viewportWidth, int viewportHeight, Uint32* pixels)
{
    for (int i = 0; i < viewportHeight; i++)
    {
        for (int j = 0; j < viewportWidth; j++)
        {
            int tileX = player.GetX() + j - (viewportWidth / 2);
            int tileY = player.GetY() + i - (viewportHeight / 2);

            // bounds checking to see whether we veered off the edge of the world map

            if (tileX < 0)
                tileX += 168;
            else if (tileX > 167)
                tileX -= 168;

            if (tileY < 0)
                tileY += 156;
            else if (tileY > 155)
                tileY -= 156;

            char tileIndex = world.GetTileIndexAt(tileX, tileY);

            // adjust tileIndex because some tiles are just alternate frames
            // of the previous tile, i.e. city and castle tiles have alternate
            // flag positions

            if (tileIndex > 4)
                tileIndex++;
            if (tileIndex > 7)
                tileIndex++;

            // animate city and castle tiles (flags waving)

            if (tileIndex == 4 || tileIndex == 7)
            {
                Uint32 ticks = SDL_GetTicks();
                Uint32 interval = ticks / 250;

                if (tileIndex == 4 && interval % 4 == 0) // castle flag is slower and symmetric
                    tileIndex++;
                else if (tileIndex == 7 && interval % 3 < 2) // town flag is faster and spends more time at the top
                    tileIndex++;
            }

            // do the actual drawing

            if (i == 4 && j == 9) // middle tile: draw player tile
                DrawTile(tileset, player.GetTransport(), pixels, j, i);
            else
                DrawTile(tileset, tileIndex, pixels, j, i);
        }
    }
}

ViewportView::ViewportView(SDL_Renderer* renderer, int width, int height,
    int viewportWidth, int viewportHeight, World* world, Tileset* tileset,
    Player* player)
    : View(renderer), width(width), height(height), viewportWidth(viewportWidth),
        viewportHeight(viewportHeight), pixels(nullptr), texture(nullptr),
        viewportRect({ 8, 8, 304, 144 }), world(world), tileset(tileset),
        player(player)
{
    pixels = new Uint32[width * height];
    texture = SDL_CreateTexture(renderer,
        SDL_PIXELFORMAT_ARGB8888, SDL_TEXTUREACCESS_STATIC, width, height);

    memset(pixels, 0, width * height * sizeof(Uint32));
}

ViewportView::~ViewportView()
{
    delete[] pixels;
    SDL_DestroyTexture(texture);
}

void ViewportView::Render()
{
    auto renderer = GetRenderer();

    SDL_UpdateTexture(texture, &viewportRect, pixels, width * sizeof(Uint32));
    DrawViewport(*world, *tileset, *player, viewportWidth, viewportHeight, pixels);
    SDL_RenderCopy(renderer, texture, NULL, NULL);
}