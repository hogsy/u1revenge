#include "SoundEffect.h"

SoundEffect::SoundEffect(const char* filename)
	: filename(filename)
{
	// TODO error handling

	SDL_LoadWAV(filename, &wavSpec, &wavBuffer, &wavLength);
}

SoundEffect::~SoundEffect()
{
	SDL_FreeWAV(wavBuffer);
}

SDL_AudioSpec SoundEffect::GetWavSpec()
{
	return wavSpec;
}

Uint8 * SoundEffect::GetWavBuffer()
{
	return wavBuffer;
}

Uint32 SoundEffect::GetWavLength()
{
	return wavLength;
}
