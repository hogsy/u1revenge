#ifndef _GAMESETTINGS_H_
#define _GAMESETTINGS_H_

#include <string>
#include <map>

using std::string;

class GameSettings
{
private:
    std::map<string, string> settings;

    string Trim(const string& str);
public:
    GameSettings();
    ~GameSettings();
    void Load(const char* filePath);
    string Get(string key, string defaultValue = "");
    int GetInt(string key, int defaultValue = -1);
};

#endif