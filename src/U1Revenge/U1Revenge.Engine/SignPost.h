#ifndef _SIGNPOST_H_
#define _SIGNPOST_H_

#include "Location.h"

class SignPost : public Location
{
public:
    SignPost(const char* name, int x, int y);
    ~SignPost();
};

#endif