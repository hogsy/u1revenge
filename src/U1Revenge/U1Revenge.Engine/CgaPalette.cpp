#include "CgaPalette.h"

CgaPalette::CgaPalette()
{
}


CgaPalette::~CgaPalette()
{
}

int CgaPalette::IndexToColour(char index)
{
    switch (index)
    {
        case 0: return 0xFF000000;
        case 1: return 0xFF00AAAA;
        case 2: return 0xFFAA00AA;
        case 3: return 0xFFAAAAAA;
        default: return 0xFF000000;
    }
}