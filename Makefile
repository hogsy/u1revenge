SDL_OK := $(shell pkg-config sdl2 --atleast-version=2.0.5; echo $$?)
ifeq ($(SDL_OK),1)
    $(error "U1Revenge needs at least SDL version 2.0.5")
endif

LIBS      := $(shell pkg-config SDL2_ttf sdl2 --libs)
SOURCEDIR := src/U1Revenge/U1Revenge.Engine
SOURCES   := $(shell find $(SOURCEDIR) -name '*.cpp')
OBJECTS   := $(SOURCES:.cpp=.o)
DEPS      := $(SOURCES:.cpp=.d)
CXX       ?= g++
PREFIX    ?= /usr/local
CXXFLAGS  += -std=c++11 $(shell pkg-config SDL2_ttf sdl2 --cflags) -DFONTPATH=$(PREFIX)/share/fonts/ -I src/U1Revenge/U1Revenge.Engine/

all: u1revenge

u1revenge: $(OBJECTS)
	$(CXX) -o u1revenge $(OBJECTS) $(LIBS)

# pull in dependency info for *existing* .o files
-include $(DEPS)

%.o: %.cpp
	$(CXX) -c $(CXXFLAGS) $< -o $@
	$(CXX) -MM $(CXXFLAGS) -MT $@ $< > $*.d

.PHONY: install
clean:
	rm $(OBJECTS) $(DEPS)

.PHONY: install
install: u1revenge
	mkdir -p $(PREFIX)/bin
	cp $< $(PREFIX)/bin/u1revenge
	mkdir -p $(PREFIX)/share/fonts
	cp res/font/pcsenior.ttf $(PREFIX)/share/fonts

.PHONY: uninstall
uninstall:
	rm -f $(PREFIX)/bin/u1revenge
	rm $(PREFIX)/share/fonts/pcsenior.ttf
