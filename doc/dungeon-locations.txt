Lands of Lord British

The Dungeon of Perinia         0x12  0x0D
The Unholy Hole                0x30  0x0B
The Dungeon of Montor          0x35  0x16
The Mines of Mt. Drash         0x3B  0x1D
Mondain's Gate to Hell         0x1D  0x25
The Lost Caverns               0x0D  0x2B
The Dungeon of Doubt           0x3E  0x31
The Mines of Mt. Drash II      0x27  0x3C
Death's Awakening              0x26  0x44

Lands of the Feudal Lords

The Savage Place               0x82  0x0A
Scorpion Hole                  0x64  0x0F
Advari's Hole                  0x7C  0x1A
The Dead Warrior's Fight       0x9B  0x23
The Horror of the Harpies      0x93  0x24
The Labyrinth                  0x62  0x2D
Where Hercules Died            0x6D  0x32
The Horror of the Harpies II   0x74  0x38
The Gorgon Hole                0x88  0x3B

Lands of Danger and Despair

The Skull Smasher              0x77  0x59
The Spine Breaker              0x95  0x5B
The Dungeon of Doom            0x72  0x64
The Dead Cat's Life            0x6C  0x6B
The Morbid Adventure           0x8A  0x73
Free Death Hole                0x9A  0x79
Dead Man's Walk                0x69  0x7F
The Dead Cat's Life II         0x80  0x8A
The Hole to Hades              0x81  0x92

Lands of the Dark Unknown

The Tramp of Doom              0x34  0x60
The Viper's Pit                0x20  0x63
The Long Death                 0x19  0x69
The End...                     0x0E  0x6E
The Viper's Pit II             0x3F  0x77
The Slow Death                 0x47  0x78
The Guild of Death             0x28  0x81
The Metal Twister              0x10  0x8C
The Troll's Hole               0x2E  0x91
