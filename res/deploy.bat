set path=%PATH%;C:\tools\7zip
set workingDir=%cd%

cd ..\src\U1Revenge\Debug

for /f "tokens=2 delims==" %%a in ('wmic OS Get localdatetime /value') do set "dt=%%a"
set "YYYY=%dt:~0,4%"
set "MM=%dt:~4,2%"
set "DD=%dt:~6,2%"
set "HH=%dt:~8,2%"
set "min=%dt:~10,2%"
set "ss=%dt:~12,2%"

set releaseDir=u1revenge-%YYYY%.%MM%.%DD%-%HH%.%min%.%ss%
mkdir %releaseDir%

copy *.dll %releaseDir%
copy U1Revenge.Engine.exe %releaseDir%
copy ..\..\..\res\font\pcsenior.ttf %releaseDir%
copy ..\..\..\res\gamesettings.txt %releaseDir%
copy ..\..\..\res\sfx\move-grass.wav %releaseDir%

7z a -r -tzip %releaseDir%.zip %releaseDir%
rmdir /S /Q %releaseDir%

cd %workingDir%
